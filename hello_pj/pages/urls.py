from django.urls import path

from . import views

urlpatterns = [
	path('', views.opened, name='opened'),
	path('felfuggesztett/', views.suspended, name='suspended'),
	path('felterjesztett/', views.petition, name='petition'),
	path('osztalyos/', views.department, name='department'),
	path('lezart/', views.closed, name='closed'),
	path('lejaro/', views.late, name='late'),
]