from django.shortcuts import render

from django.http import HttpResponse

# Create your views here

def opened(request):
    context = {"opened_page": "active"}
    return render(request, 'pages/nyitott.html', context)
	
def suspended(request):
    context = {"suspended_page": "active"}
    return render(request, 'pages/felfuggesztett.html', context)
	
def petition(request):
    context = {"petition_page": "active"}
    return render(request, 'pages/felterjesztett.html', context)
	
def department(request):
    context = {"department_page": "active"}
    return render(request, 'pages/osztalyos.html', context)
	
def closed(request):
    context = {"closed_page": "active"}
    return render(request, 'pages/lezart.html', context)
	
def late(request):
    context = {"late_page": "active"}
    return render(request, 'pages/lejaro.html', context)

